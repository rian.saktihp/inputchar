#!/bin/bash

echo "================================================="
echo "INPUT anything"
echo -n "Input: "; read var
if ! ([[ $var == M* ]] || [[ $var == m* ]]);
then
    if [ ${#var} -gt 5 ]
    then
    	echo Too long - 5 characters max
    	exit 0
    else
    	echo Your input is "$var"
    fi
else
    echo Fail
fi
